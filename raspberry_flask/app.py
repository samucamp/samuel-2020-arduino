from flask import Flask, request
import json

from models.arduino_serial_write import changeLedArduino as led

app = Flask(__name__)


@app.route('/')
def home():
    return '<h1>Home</h1>'


@app.route("/led")
def changeLED():
    y = json.loads(request.data)
    print(y["cor"])
    led(y["cor"])
    return json.dumps(y)


@app.route("/rgb")
def changeRGB():
    y = json.loads(request.data)
    print(y["red"], y["blue"], y["green"])
    return y


if __name__ == "__main__":
    app.run(debug=True)
    # debug=True is set to allow possible errors to appear on the web page.
    # IMPORTANT POINT: This shall never be used in production environment
