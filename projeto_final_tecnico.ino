/* Tabela que relaciona os bot�es e fun��es com seus respectivos 
caracteres enviados pelo android com suas vari�veis utilizadas no arduino
e seu nome no comando de voz.

Manual:  b 	/ manual = 1 
Retornar: dimmer4 	/ manual = 0 
slider1 : dimmer1 valor
slider2 : dimmer2 valor
slider3 : dimmer3 valor

Controle : u	/ modo = 5
Automatico:  a  	/ modo = 4	(Autom�tico, Boate)
Fade:  d 		/ modo = 3	(Fade, Variar)
Flash:  c 		/ modo = 2	(Piscar, Pisca)
Apagar: e		/ modo =  1 	(Apagar)
	
Red:  g 		/ cor = 1		(Vermelho)
Blue:  k 		/ cor = 2		(Azul)
Green:  i 		/ cor = 3		(Verde)
Magenta:	f	/ cor = 4		(Rosa, Magenta)
Cyan:  j 		/ cor = 5		(Ciano)
Yellow:  h		/ cor = 6		(Amarelo)
White:  l 		/ cor = 7 		(Branco)

Ligar:  m 		/ cooler = 1	(Ligar, Cooler)
Desligar:  n 	/ cooler = 0	(Desligar)
Ligar2: o 		/ onoff2 = 1	
Desligar2:  p 	/ onoff2 = 0
Ligar3: q 		/ onoff3 = 1
Desligar3: r 	/ onoff3 = 0 
Ligar4: s 		/ onoff4 = 1
Desligar4: t 	/ onoff4 = 0 

Desliga Geral : v 	/ desligaGeral = 1 */	//(Desliga Geral)

#include <Keypad.h>                                   //Biblioteca para teclado membrana

#define RED 5
#define BLUE 6
#define GREEN 7

int VM = 0; //variavel manipulada
int VP = 0; //variavel do processo
int e;  //erro
int pI; //c�lculo da integral
//par�metros de controle
int kp;
int ki;
int sp;

int count = 0;                                        //Contador de uso geral
char pass [4] = {'1', '2', '3', '4'};                 //Senha
const int duration = 200;                             //Dura��o dos sons tocados // buzzer
const byte ROWS = 4;                                  //Quatro linhas // teclado membrana
const byte COLS = 3;                                  //Tr�s colunas

//Mapeamento de teclas
char keys[ROWS][COLS] = {
  {'1','2','3'},
  {'4','5','6'},
  {'7','8','9'},
  {'*','0','#'}
};

byte rowPins[ROWS] = {31, 33, 35, 37};                 //Defini��o de pinos das linhas do teclado membrana
byte colPins[COLS] = {39, 41, 43};                     //Defini��o de pinos das colunas

String stringGeral, dispositivo, valorRecebido;        //Leitura string do dimmer
int dimmer;                                            //Valor num�rico do dimmer
int cor = 0;                                           //Vari�vel que guarda a cor
int modo = 0;                                          //Vari�vel que guarda o modo
int cooler = 0;                                        //Estado do cooler
int manual = 0;                                        //Estado (1 = entrou no modo manual)

String stringControle;                                 //Leitura string de controle de luminosidade
int valorSetpoint;                                     //valor Int da string do setpoint
int auxControle = 0;                                   //(1 = entrou no modo controle luminosidade)

//Estado das sa�das extras
int onoff2 = 0;                                     
int onoff3 = 0;
int onoff4 = 0;

//auxiliar para Serial
int serialOn = 0; 

int aux;                                               //Vari�vel que guarda cor anterior
char palavra;                                          //Recebe a leitura serial
float celsius = 0.4887585532746823069403714565;        //convers�o para graus celsius
int temperatura;                                       //valor do sensor de temperatura
int i;                                                 //auxiliar para leitura da temperatura
float soma;                                            //soma para os valores de temperatura
int desligaGeral = 1;                                  //diz se o sistema est� liberado ou n�o  
unsigned long previousMillis = 0;                      //define variavel que recebe tempo anteior
const long interval = 1000;                            //intervalo para delay
int audioPin = 8;                                      //pino do buzzer

//Cria o teclado
Keypad keypad = Keypad( makeKeymap(keys), rowPins, colPins, ROWS, COLS);  

//Defini��es iniciais
void setup(){
  Serial.begin(9600); //Inicia comunica��o serial
  
  //pinMode OUTPUT -> Define como sa�da
  pinMode(RED, OUTPUT);
  pinMode(BLUE, OUTPUT);
  pinMode(GREEN, OUTPUT);
  pinMode(12,OUTPUT); //Cooler
  pinMode(11,OUTPUT); //Sa�da extra 1
  pinMode(10,OUTPUT); //Sa�da extra 2
  pinMode(9,OUTPUT); //Sa�da extra 3
  pinMode(audioPin, OUTPUT); //Buzzer pino 8
  key_init();      //Inicializa o sistema

  
  //Inicializa��o dos par�metros controlador luminosidade PI;
  sp = 100;  //  0 <= SP <= 100 
  kp = 2;
  ki = 0.5;
}

void verificaSerial(){          //Recebe as letras (palavra) via bluetooth e atribui valores parar as vari�veis
  palavra = Serial.read();  
  if(palavra == 'g')  {cor = 1; modo = 0; aux = 1;}
  else if(palavra == 'k')  {cor = 2; modo = 0; aux = 2;}
  else if(palavra == 'i')  {cor = 3; modo = 0; aux = 3;}
  else if(palavra == 'f')  {cor = 4; modo = 0; aux = 4;}
  else if(palavra == 'j')  {cor = 5; modo = 0; aux = 5;}
  else if(palavra == 'h')  {cor = 6; modo = 0; aux = 6;}
  else if(palavra == 'l')  {cor = 7; modo = 0; aux = 7;}
  if(palavra == 'e')  {modo = 1; cor = 0;}
  else if(palavra == 'c')  {cor = aux; modo = 2;}
  else if(palavra == 'd')  {modo = 3; cor = 8;} 
  //cor 8 representa "nenhuma cor", pois a fun��o que utiliza cor=8, tem valores pr�prios para a ilumina��o;
  else if(palavra == 'a')  {modo = 4; cor = aux; cooler = 1; onoff2 = 1;}
  else if(palavra == 'u') {modo = 5; cor = 8;}
  if(palavra == 'm')  cooler = 1;
  else if(palavra == 'n') cooler = 0;
  if(palavra == 'o')  onoff2 = 1;
  else if(palavra == 'p') onoff2 = 0;
  if(palavra == 'q')  onoff3 = 1;
  else if(palavra == 'r') onoff3 = 0;
  if(palavra == 's')  onoff4 = 1;
  else if(palavra == 't') onoff4 = 0;
  if(palavra == 'b') {manual = 1; modo = 0; cor = 8;}
  if(palavra == 'v') {desligaGeral = 1; modo = 0; onoff2 = 0; onoff3 = 0; onoff4 = 0; cooler = 0; cor = 0;}
  //Desliga Geral ( v )(qnd apertar = 1 = tem que por senha) ( 0 = loop liberado)
}

//Muda a cor da fita led conforme o valor de "cor"
void mudaCor(){
  if(cor == 7){analogWrite(RED,255);  analogWrite(BLUE,255);  analogWrite(GREEN,255);}
  else if(cor == 1){analogWrite(RED,255); analogWrite(BLUE,0); analogWrite(GREEN,0);}
  else if(cor == 2){analogWrite(RED,0); analogWrite(BLUE,255);  analogWrite(GREEN,0);}
  else if(cor == 3){analogWrite(RED,0); analogWrite(BLUE,0);  analogWrite(GREEN,255);}
  else if(cor == 4){analogWrite(RED,255); analogWrite(BLUE,255);  analogWrite(GREEN,0);}
  else if(cor == 5){analogWrite(RED,0); analogWrite(BLUE,255);  analogWrite(GREEN,255);}
  else if(cor == 6){analogWrite(RED,255); analogWrite(BLUE,0);  analogWrite(GREEN,255);}
  else if(cor == 0){analogWrite(RED,0); analogWrite(BLUE,0);  analogWrite(GREEN,0);} 
}

//Fun��o Fade (variar)
void fade(){ 
  int r, g, b;
  if (modo == 3){ r = 0;  while(r <= 255 && modo == 3){  analogWrite(RED,r);  delay(1);  r++;  verificaSerial();  enviaTemperatura();  onOff();}}
  if (modo == 3){ b = 255;  while(b >= 0 && modo == 3){  analogWrite(BLUE,b);  delay(1);  b--;  verificaSerial();  enviaTemperatura();  onOff();}} 
  if (modo == 3){ g = 0;  while(g <= 255 && modo == 3){  analogWrite(GREEN,g);  delay(1);  g++;  verificaSerial();  enviaTemperatura();  onOff();}}
  if (modo == 3){ r = 255;  while(r >= 0 && modo == 3){  analogWrite(RED,r);  delay(1);  r--;  verificaSerial();  enviaTemperatura();  onOff();}}
  if (modo == 3){ b = 0;  while(b <= 255 && modo == 3){  analogWrite(BLUE,b);  delay(1);  b++;  verificaSerial();  enviaTemperatura();  onOff();}} 
  if (modo == 3){ g = 255;  while(g >= 0 && modo == 3){  analogWrite(GREEN,g);  delay(1);  g--;  verificaSerial();  enviaTemperatura();  onOff();}}
  if (desligaGeral == 1) {cor = 0; mudaCor();}
}

//Blink(piscar)
void flash(){
  delay(50);
  analogWrite(RED,0);   
  analogWrite(GREEN,0);
  analogWrite(BLUE,0);
  delay(50);
}

//Fun��o que faz leitura e envia o valor da temperatura parar o celular
void enviaTemperatura() {
  unsigned long currentMillis = millis();
  soma = 0;
  
  for(i=0;i<=100;i++){soma += analogRead(0);} //Leitura da m�dia de 100 medidas da temperatura
  temperatura = (soma/100) * celsius;

  if(currentMillis - previousMillis >= interval) { //Delay utilizando millis()
    previousMillis = currentMillis;   
      Serial.print(temperatura);
      Serial.println("");
  }
}

//Fun��o que liga e desliga as sa�das digitais utilizadas
void onOff(){
  if(cooler == 1)  digitalWrite(12, HIGH);
  else if(cooler == 0)  digitalWrite(12, LOW);
  if(onoff2 == 1)  digitalWrite(11, HIGH);
  else if(onoff2 == 0)  digitalWrite(11, LOW);
  if(onoff3 == 1)  digitalWrite(10, HIGH);
  else if(onoff3 == 0)  digitalWrite(10, LOW);
  if(onoff4 == 1)  digitalWrite(9, HIGH);
  else if(onoff4 == 0)  digitalWrite(9, LOW);
}

//Separa o nome do slider de seu valor
void divideDados(){
  if(stringGeral.indexOf(" ")>=0){
    //Recebe o que esta antes do espa�o. Exemplo [dimmer1 255], recebe dimmer1
    dispositivo = stringGeral.substring(0,(stringGeral.indexOf(" ")));
    //Recebe o que esta depois do espa�o 
    valorRecebido = stringGeral.substring(stringGeral.indexOf(" ")+1);
  } 
}

//L� o comando do slider enviado pelo celular e aplica o PWM na fita LED
void slider(){
  while(manual == 1){
    if(Serial.available()){
      stringGeral = String("");
      while(Serial.available()){stringGeral = stringGeral + char(Serial.read()); delay(1);} //Recebe a string do slider enviada pelo celular
      divideDados(); 
      dimmer = valorRecebido.toInt(); //Converte para inteiro
      if(dispositivo == "dimmer1")  analogWrite(GREEN, dimmer);
      if(dispositivo == "dimmer2")  analogWrite(RED, dimmer);
      if(dispositivo == "dimmer3")  analogWrite(BLUE, dimmer);
      if(dispositivo == "dimmer4")  manual = 0;
    }
  }
}

//Fun��o "modo boate" que aplica efeitos na fita led, liga cooler e sa�da extra 1
void automatico(){
  onOff();
  for(cor=0;cor<=8;cor++){  mudaCor();  delay(100);  verificaSerial();  onOff();  enviaTemperatura();}
  delay(200);    
  int r, g, b;
  if (modo ==  4){  r = 0;  while(r <= 255 && modo == 4){  analogWrite(RED,r);  delay(1);  r++;  verificaSerial();  onOff();}}
  enviaTemperatura();
  if (modo ==  4){  b = 255;  while(b >= 0 && modo == 4){  analogWrite(BLUE,b);  delay(1);  b--;  verificaSerial();  onOff();}} 
  enviaTemperatura();
  if (modo ==  4){  g = 0;  while(g <= 255 && modo == 4){  analogWrite(GREEN,g);  delay(1);  g++;  verificaSerial();  onOff();}}
  enviaTemperatura  ();
  if (modo ==  4){r = 255;  while(r >= 0 && modo == 4){  analogWrite(RED,r);  delay(1);  r--;  verificaSerial();  onOff();}}
  enviaTemperatura();
  if (modo ==  4){  b = 0;  while(b <= 255 && modo == 4){  analogWrite(BLUE,b);  delay(1);  b++;  verificaSerial();  onOff();}}
  enviaTemperatura();
  if (modo ==  4){  g = 255;  while(g >= 0 && modo == 4){  analogWrite(GREEN,g);  delay(1);  g--;  verificaSerial();  onOff();}}
  onOff();
  cor = 0;
  mudaCor();
}

//Subrotina para Acesso Liberado 
void unlocked(){
  desligaGeral = 0;
  onoff2 = 1;
  tone(audioPin, 2000, 100);
  delay(duration);
  noTone(audioPin);
  tone(audioPin, 2000, 100);
  delay(duration);
  noTone(audioPin);
  delay(250); 
}

//Subrotina de Entrada da Senha 
void code_entry_init(){ 
  count = 0;                                                //Vari�vel count � zero na entrada de senha
  //Emite som e acende LEDs
  tone(audioPin, 1500, 100);
  delay(duration);
  noTone(audioPin);
  tone(audioPin, 1500, 100);
  delay(duration);
  noTone(audioPin);
  tone(audioPin, 1500, 100);
  delay(duration);
  noTone(audioPin);
}

//Inicializar o Sistema de Senha
void key_init (){
  count = 0;                                                //Vari�vel count � zero na inicializa��o
  
  //Emite som , indicando Sistema Iniciado
  tone(audioPin, 1080, 100);
  delay(duration);
  noTone(audioPin);
  tone(audioPin, 980, 100);
  delay(duration);
  noTone(audioPin);
  tone(audioPin, 770, 100);
  delay(duration);
  noTone(audioPin);
}

void calculoIntegral(){
  unsigned long lastTime; 
  unsigned long now = millis();
  double timeChange = (double)(now - lastTime);
  
  pI += (double)(e * timeChange);

  lastTime = now;
}

//Fun��o que realiza o controle proporcional integral
void controlePI(){
  //potenci�metro 
  sp = map(analogRead(2), 0, 1023, 100, 0); 

  VP = map(analogRead(1), 700, 1000, 0, 100);
  //satura��o dos valores em porcentagem de 0 a 100
  if(VP > 100) VP = 100;
  if(VP < 0) VP = 0;

  if(sp > 100) sp = 100;
  if(sp < 0) sp = 0;
  //100 = claro, 0 = escuro

  e = sp - VP;
  if(e > 100) e = 100;
  if(e < 0) e = 0;
  
  calculoIntegral();   

  //calculo do valor de sa�da
  VM = (double) 100 - kp*(e + (ki*pI));
  if(VM > 100) VM = 100;
  if(VM < 0) VM = 0;
  //mapeamento para a porta pwm
  VM = map(VM, 0, 100, 0, 255);
  
  if (sp >= 95){analogWrite(RED, 0);analogWrite(BLUE, 0);  analogWrite(GREEN, 0);}
  else{analogWrite(RED, VM);  analogWrite(BLUE, VM);  analogWrite(GREEN, VM);  
  }
}


//Loop principal
void loop(){
//inicializa desligaGeral = 1, se senha correta, desligaGeral = 0, se click Desliga Geral, desligaGeral = 1;

  if (desligaGeral ==1){
    if(serialOn == 0){ Serial.begin(0); serialOn = 1;}  //Condi��o para executar Serial(0) uma vez
    char key = keypad.getKey();                         //Obt�m tecla pressionada
    if (key != NO_KEY){                                 //Se foi pressionada uma tecla:
      if (key == '#') {                                 //Se a tecla � '#'
        code_entry_init();                              //Ent�o espera que seja inserida uma senha
        int entrada = 0;
        while (count < 4 ){                             //Conta 4 entradas/teclas
          char key = keypad.getKey();                   //Obt�m tecla pressionada
          if (key != NO_KEY){                           //Se foi pressionada uma tecla: 
            entrada += 1;                               //Faz entrada = entrada + 1
            tone(audioPin, 1080, 100);                  //Para cada d�gito emite um som de indica��o
            delay(duration);                            //Dura��o do som
            noTone(audioPin);                           //Para de emitir som
            if (key == pass[count])count += 1;          //Se a tecla pressionada corresponde ao d�gito //da senha correspondente, soma 1 no contador
            if ( count == 4 ) unlocked();               //Se contador chegou a 4 e com d�gitos corretos //desbloqueia sistema
            if ((key == '#') || (entrada == 4)){        //Se foi pressionada a tecla "#' ou foram feitas //4 entradas
              key_init();                               //Inicializa o sistema
            break;                                      //Para o sistema e espera por uma tecla
            }
          }
        }
      }
    }
  }

  if(desligaGeral == 0){
    if(serialOn == 1){ Serial.begin(9600); serialOn = 0;} ////Condi��o para executar Serial(9600) uma vez
    verificaSerial();  
    mudaCor();
    enviaTemperatura();
    onOff();
    if(modo == 2)  flash();
    else if(modo == 3)  fade();
    else if(modo == 4)  automatico();
    else if(modo == 5)  controlePI();
    if(manual ==1)  slider();}
}