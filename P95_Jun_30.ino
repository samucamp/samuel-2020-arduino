//
//ALGORITMOS E ESTRUTURAS DE DADOS LISTA 02
//SAMUEL MARTINS PERES 
//
//P095
//Faca um programa para inverter os caracteres contidos em uma cadeia de caracteres.

char *cadeia_caracter[10] = {"a", "l", "g", "o", "r", "i", "t", "m", "o", "s"};

void setup() {
  Serial.begin(9600);
  Serial.println(" ");  
  for (int i=0; i<10; i++){
    Serial.print(cadeia_caracter[i]);
  }

  Serial.println(" ");
  
  for (int i=9; i>=0; i--){
    Serial.print(cadeia_caracter[i]);
  }
  Serial.println(" ");
}
void loop() {
}
