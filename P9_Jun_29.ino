//
//ALGORITMOS E ESTRUTURAS DE DADOS LISTA 02
//SAMUEL MARTINS PERES 
//
//P009
//O custo ao consumidor de um carro novo, e a soma do custo de fabricac~ao com o
//percentual do distribuidor e dos impostos (aplicados ao custo de fabricac~ao). Supondo
//que o percentual do distribuidor seja de 28% e os impostos de 45%, escreva um programa
//para ler o custo de fabricac~ao de um carro e exibir o custo nal ao consumidor.

float custo_fabricacao = 20000;
float percent_distribuidor = 28;
float percent_impostos = 45;
float preco_final=0;

void setup() {
  Serial.begin(9600);
  delay(500);

  Serial.print("O custo de fabricacao eh: ");
  Serial.print(custo_fabricacao);
  Serial.println(" reais");
  Serial.print("O percentual do distribuidor eh: ");
  Serial.print(percent_distribuidor);
  Serial.println("%");
  Serial.print("O percentual de impostos eh: ");
  Serial.print(percent_impostos);
  Serial.println("%");

  preco_final = custo_fabricacao + (custo_fabricacao*percent_distribuidor/100) + (custo_fabricacao*percent_impostos/100);
  Serial.print("O valor do carro eh: ");
  Serial.print(preco_final);
  Serial.println(" reais");
}

void loop() {
}
