//
//ALGORITMOS E ESTRUTURAS DE DADOS LISTA 02
//SAMUEL MARTINS PERES
//
//P035
//Um posto esta vendendo combustveis com a seguinte tabela de descontos:
//Alcool:
//ate 20 litros desconto de 3% por litro.
//acima de 20 litros desconto de 5% por litro.
//Gasolina:
//ate 20 litros desconto de 4% por litro.
//acima de 20 litros desconto de 6% por litro.
//Escreva um programa que leia o numero de litros vendidos, o tipo de combustivel
//(codificado da seguinte forma: A-alcool, G-gasolina), calcule e exiba o valor a ser pago
//pelo cliente sabendo-se que o preco da gasolina e de R$ 3,20 o litro e o alcool R$ 2,90.

int combustivel;
float preco_G = 3.2;
float preco_A = 2.9;
float preco = 0;
int litros = 0;

void setup() {
  Serial.begin(9600);
  delay(2000);
}

void loop() {
  Serial.println("Digite o código.");
  while(Serial.available()==0){}
  combustivel = Serial.read();
  
  switch (combustivel) {
    case 'A':
        Serial.print("Digite quantos litros vendidos: ");
        while(Serial.available()==0){}
        litros = Serial.parseInt();
        Serial.println(litros); 
        if (litros <= 20){
          preco = preco_A * litros * (1 + 3.0/100);
        } else {
          preco = preco_A * litros * (1 + 5.0/100);
        }
        Serial.print("O preco total é: ");
        Serial.println(preco);
     break;
    case 'G':
        Serial.print("Digite quantos litros vendidos: ");
          while(Serial.available()==0){}
          litros = Serial.parseInt();
          Serial.println(litros); 
          if (litros <= 20){
            preco = preco_G * litros * (1 + 4.0/100);
          } else {
            preco = preco_G * litros * (1 + 6.0/100);
          }
          Serial.print("O preco total é: ");
          Serial.println(preco);
      break;
    default: 
      Serial.println("Código incorreto."); 
    break;
  }
}

