#include <Wire.h>
#include <LiquidCrystal_I2C.h>

// Inicializa o display no endereco 0x27
LiquidCrystal_I2C lcd(0x27,2,1,0,4,5,6,7,3, POSITIVE);
 
//Pinagem ponte H
int IN1 = 4;
int INV = 5;

//Pinagem Encoder
int pino_D0 = 2;
int rpm;
volatile byte pulsos;
unsigned long timeold;
unsigned int pulsos_por_volta = 20; //Altere de acordo com o disco

//Pinagem joystick
int pino_x = A2; //Pino ligado ao X do joystick
int pino_y = A3; //Pino ligado ao Y do joystick
int val_x;   //Armazena o valor do eixo X
int val_y;   //Armazena o valor do eixo Y

//Função para incrementar encoder
void contador(){  pulsos++;  }

void setup(){
 //Serial.begin(9600);
 lcd.begin (16,2);
 lcd.setBacklight(HIGH);
 pinMode(IN1, OUTPUT);
 pinMode(INV, OUTPUT);
 pinMode(pino_D0, INPUT); //Interrupcao 0 - pino digital 2
 attachInterrupt(0, contador, FALLING); //Aciona o contador a cada pulso
 pulsos = 0;
 rpm = 0;
 timeold = 0;
 }
  
void loop()
{ 
  val_x = analogRead(pino_x);
  val_x = map(val_x, 0, 1023, 150, 255);
  analogWrite(INV, val_x);
  digitalWrite(IN1, LOW);
/*
  if (val_x <= 3 && val_x >= -7){
   //Para o motor A
    digitalWrite(IN1, HIGH);
    digitalWrite(IN2, HIGH);
  }else if(val_x > 3){
  //Gira o Motor A no sentido horario
    digitalWrite(IN1, HIGH);
    digitalWrite(IN2, LOW);
  }else if(val_x < -7){
 //Gira o Motor A no sentido anti-horario
    digitalWrite(IN1, LOW);
    digitalWrite(IN2, HIGH);    
  }
*/
  //Atualiza contador a cada segundo
  if (millis() - timeold >= 1000)  {
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("RPM: ");
  lcd.print(rpm);
  //lcd.setCursor(0,1);
  //lcd.print("PWM: ");
  //lcd.print(val_x);
  //Serial.println("RPM: "); Serial.println(rpm);
  //Desabilita interrupcao durante o calculo
  detachInterrupt(0);
  //rpm = (60 * 1000 / pulsos_por_volta ) / (millis() - timeold) * pulsos;
  rpm = (60 * pulsos / pulsos_por_volta );
  timeold = millis();
  pulsos = 0;
  //Habilita interrupcao
  attachInterrupt(0, contador, FALLING);  
  }
 }


 
