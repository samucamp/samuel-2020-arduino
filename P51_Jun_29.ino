//
//ALGORITMOS E ESTRUTURAS DE DADOS LISTA 02
//SAMUEL MARTINS PERES 
//
//P051
//Utilize a estrutura if para fazer um programa que exiba o nome de um produto a partir
//do codigo do mesmo.
//Considere os seguintes codigos:
//001 Parafuso
//002 Porca
//003 Prego
//Para qualquer outro codigo indicar ``Diversos''.

int codigo = 000;

void setup() {
  Serial.begin(9600);
}

void loop() {
  Serial.print("Digite o codigo: ");
  while ( Serial.available() == 0){}
  codigo = Serial.parseInt();
  Serial.println(codigo);

  if (codigo == 001){
    Serial.println("Parafuso");
  }else if (codigo == 002){
    Serial.println("Porca");
  }else if (codigo == 003){
    Serial.println("Prego");
  }else{
    Serial.println("Diversos");
  }
}
