//Bibliotecas 
#include <Wire.h>
#include <LiquidCrystal_I2C.h>        //Esta biblioteca tem 2 vers�es uma est� apropriada para o IDEv1.0
#include <Keypad.h>

//Inicializa��o do Display I2C
LiquidCrystal_I2C lcd(0x27,16,2);


int count = 0;                                              //Contador de uso geral
char pass [4] = {'1', '2', '3', '4'};                 //Senha
const int yellowPin = 11;                             //Defini��o do pino do LED Amarelo
const int redPin = 12;                                 //Defini��o do pino do LED Vermelho
const int greenPin = 10;                              //Defini��o do pino do LED Verde
const int audioPin = 9;                                //Defini��o do pino do Buzzer
const int duration = 200;                             //Dura��o das sons tocados
const byte ROWS = 4;                              //Quatro linhas
const byte COLS = 3;                               //Tr�s colunas

//Mapeamento de teclas
char keys[ROWS][COLS] = {
  {'1','2','3'},
  {'4','5','6'},
  {'7','8','9'},
  {'*','0','#'}
};
byte rowPins[ROWS] = {5, 4, 3, 2};                    //Defini��o de pinos das linhas
byte colPins[COLS] = {6, 7, 8};                          //Defini��o de pinos das colunas

//Cria o teclado
Keypad keypad = Keypad( makeKeymap(keys), rowPins, colPins, ROWS, COLS );
//Programa inicial 
void setup(){
  lcd.init();                                              //Inicializa o LCD
  lcd.backlight();                                     //Com Backlight
  Serial.begin(9600);                              //Inicializa a comunica��o serial a 9600 bps
  
  //Deifini��o de modo dos pinos
  pinMode(audioPin, OUTPUT);
  pinMode(yellowPin, OUTPUT);
  pinMode(redPin, OUTPUT);
  pinMode(greenPin, OUTPUT);
  lcd.clear();                                        //Limpa LCD
  key_init();                                         //Inicializa o sistema
}



//Loop Principal do Programa 
void loop(){
  char key = keypad.getKey();                         //Obt�m tecla pressionada
  if (key != NO_KEY){                                  //Se foi pressionada uma tecla:
    if (key == '#') {                                          //Se a tecla � '#'
      code_entry_init();                                    //Ent�o espera que seja inserida uma senha
      int entrada = 0;
      while (count < 4 ){                                  //Conta 4 entradas/teclas
        char key = keypad.getKey();                //Obt�m tecla pressionada
        if (key != NO_KEY){                          //Se foi pressionada uma tecla:
          entrada += 1;                                     //Faz entrada = entrada + 1
          tone(audioPin, 1080, 100);                 //Para cada d�gito emite um som de indica��o
          delay(duration);                                 //Dura��o do som
          noTone(audioPin);                            //Para de emitir som
          if (key == pass[count])count += 1;       //Se a tecla pressionada corresponde ao d�gito 
                                                                      //da senha correspondente, soma 1 no contador
          if ( count == 4 ) unlocked();                 //Se contador chegou a 4 e com d�gitos corretos, 
                                                                     //desbloqueia siatema
          if ((key == '#') || (entrada == 4)){        //Se foi pressionada a tecla "#' ou foram feitas
                                                                     //4 entradas,
             key_init();                                         //Inicializa o sistema
            break;                                               //Para o sistema e espera por uma tecla
          }
        }
      }
    }
  }
}


//Inicializar o Sistema 
void key_init (){
  lcd.clear();                                                //Limpa o LCD
  lcd.print("Aguardando...");                        //Emite mensagem
  lcd.setCursor(0,1);                                   //Muda de linha
  lcd.print("Tecle #");                                  //Emite mensagem
  
  count = 0;                                                //Vari�vel count � zero na inicializa��o
  
  //Emite som e acende LED Vermelho, indicando Sistema Iniciado
  digitalWrite(redPin, HIGH);
  digitalWrite(yellowPin, LOW);
  digitalWrite(greenPin, LOW);
  tone(audioPin, 1080, 100);
  delay(duration);
  noTone(audioPin);
  tone(audioPin, 980, 100);
  delay(duration);
  noTone(audioPin);
  tone(audioPin, 770, 100);
  delay(duration);
  noTone(audioPin);
}

//Subrotina de Entrada da Senha 
void code_entry_init(){
  lcd.clear();                                                //Limpa LCD
  lcd.print("Entre a Senha:");                        //Emite mensagem
  
  count = 0;                                                //Vari�vel count � zero na entrada de senha
  
  //Emite som e acende LEDs
  tone(audioPin, 1500, 100);
  delay(duration);
  noTone(audioPin);
  tone(audioPin, 1500, 100);
  delay(duration);
  noTone(audioPin);
  tone(audioPin, 1500, 100);
  delay(duration);
  noTone(audioPin);
  digitalWrite(redPin, LOW);                            //Apaga LED Vermelho
  digitalWrite(yellowPin, HIGH);                      //Acende LED Amarelo
  digitalWrite(greenPin, LOW);                        //Apaga LED Verde
}


//Subrotina para Acesso Liberado 
void unlocked(){
  lcd.clear();                                                    //Limpa LCD
  lcd.print("Acesso Liberado!");                       //Emite mensagem
  
  digitalWrite(redPin, LOW);                           //Apaga LED Vermelho
  digitalWrite(yellowPin, LOW);                      //Apaga LED Amarelo
  
  //Executa 20 vezes +- 5 segundos, emite som e pisca LED verde
  for (int x = 0; x < 20; x++){ 
  digitalWrite(greenPin, HIGH);
  tone(audioPin, 2000, 100);
  delay(duration);
  noTone(audioPin);
  digitalWrite(greenPin, LOW);
  tone(audioPin, 2000, 100);
  delay(duration);
  noTone(audioPin);
  delay(250);
  }
}