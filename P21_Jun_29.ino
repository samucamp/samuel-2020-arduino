//
//ALGORITMOS E ESTRUTURAS DE DADOS LISTA 02
//SAMUEL MARTINS PERES 
//
//P021
//Faca um programa para ler o ano atual e o ano de nascimento de uma pessoa. Exiba
//uma mensagem que diga se ela podera ou n~ao votar este ano (n~ao e necessario considerar
//o m^es em que a pessoa nasceu).

int ano_atual;
int ano_nascimento;
int idade;

void setup() {
  Serial.begin(9600);
  
  Serial.print("Digite o ano atual: ");
  while(Serial.available() == 0 ){}
  ano_atual = Serial.parseInt();
  Serial.println(ano_atual);
  
  Serial.print("Digite o ano de nascimento: ");
  while(Serial.available() == 0 ){}
  ano_nascimento = Serial.parseInt();
  Serial.println(ano_nascimento);

  idade = ano_atual - ano_nascimento;
  Serial.print("A pessoa tem ");
  Serial.print(idade);
  Serial.println(" anos.");
  
  if (idade >=16)  Serial.println("A pessoa pode votar");
  else Serial.println("A pessoa nao pode votar");
}

void loop() {

}
