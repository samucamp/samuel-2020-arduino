//
//ALGORITMOS E ESTRUTURAS DE DADOS LISTA 02
//SAMUEL MARTINS PERES 
//
//P025
//Faca um programa para ler 3 valores (considere que n~ao ser~ao informados valores iguais)
//e escrever o maior deles.

// OBS, FOI PEGO A RESOLUCAO DO EX P15 PARA ESSE PROBLEMA

int A = 23;
int B = 70;
int C = 10;
int AUX = 0;

void setup() {
  Serial.begin(9600);

  if (B > C){
    AUX = C;
    C = B;
    B = AUX;
  }
  if (A > B) {
    AUX = B;
    B = A;
    A = AUX;
  } 
  if (B > C){
    AUX = C;
    C = B;
    B = AUX;
  }
  
  Serial.print("O maior valor dos tres numeros eh: ");
  Serial.println(C);
}

void loop() {
}
