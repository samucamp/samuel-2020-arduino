const dotenv = require('dotenv');

/* ***************
Create a file .env with content below:
PORT=3000
HOST=127.0.0.1
API_KEY=**************************
API_URL=**************************
**************** */

dotenv.config();

module.exports = {
    endpoint: process.env.API_URL,
    masterKey: process.env.API_KEY,
    port: process.env.PORT,
    host: process.env.HOST
};