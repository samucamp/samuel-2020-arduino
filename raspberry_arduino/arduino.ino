//Pinagem joystick
int pino_x = A2; //Pino ligado ao X do joystick
int pino_y = A3; //Pino ligado ao Y do joystick
int val_x;   //Armazena o valor do eixo X
int val_y;   //Armazena o valor do eixo Y

void setup(){ 
  pinMode(11, OUTPUT);
  Serial.begin(115200);
  Serial.println('a');
  char a = 'b';
  while(a != 'a'){
      //waint for specific character from PC (handshake)
      a = Serial.read();
  }
 }

void loop(){ 
  //check serial data sent by PC
  if (Serial.available() > 0){
    int mode = Serial.read();
    switch (mode) {
      case 'T':{
        float analogValue = getAnalogValue();
        Serial.println(analogValue);
        break;
      }
      case '1':
        digitalWrite(11, HIGH);
        break;
      case '0':
        digitalWrite(11, LOW);
        break;
    }
  }
}

float getAnalogValue(){
  val_x = analogRead(pino_x);
  val_x = map(val_x, 0, 1023, 150, 255);
//  val_y = analogRead(pino_y);
//  val_y = map(val_y, 0, 1023, 150, 255);
  return val_x;
}