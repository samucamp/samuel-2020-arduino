const SerialPort = require("serialport");
const io = require('socket.io-client');

socket = io.connect('http://127.0.0.1:3000');

const serialPort = new SerialPort("/dev/ttyUSB0", {
    baudRate: 115200
});

const Readline = require('@serialport/parser-readline');
const parser = serialPort.pipe(new Readline({
    delimiter: '\n'
}));

serialPort.open(() => {
    console.log('Serial port connected');
});

serialPort.on('data', (data) => {
    if (data === 'a') handshake();
    else socket.emit('temp', {
        temp: data
    });
})

function handshake() {
    serialPort.write('a');

    socket.emit('init');

    setInterval(() => {
        serialPort.write('T');
    }, 500);
}

socket.on('LED', (data) => {
    if (data) serialPort.write('1');
    else serialPort.write('0');
})

socket.on('disconnect', () => {
    serialPort.write('0');
    serialPort.close();
})

// setInterval(() => {
//     port.write('red\n', (err) => console.log('Error on write: ', err));
//   console.log('message written');
//   }, 5000);

//   parser.on('data', data =>{
//     console.log('got word from arduino:', data);
//   });

// ANOTHER OPTION FOR PARSER
// const Readline = require('@serialport/parser-readline');
// const parser = serialPort.pipe(new Readline({
//     delimiter: '\n'
// }));