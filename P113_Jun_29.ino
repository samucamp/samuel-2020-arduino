//
//ALGORITMOS E ESTRUTURAS DE DADOS LISTA 02
//SAMUEL MARTINS PERES 
//
//P113
//Faca um programa para ler 10 valores inteiros e os coloque em um vetor. Em seguida
//exiba-os em ordem inversa a ordem de entrada.

int vetor[10] = {0};

void setup() {
  Serial.begin(9600);
}

void loop() {
  for (int i = 0; i < 10; i++){
    Serial.print("Digite um numero: ");
    while(Serial.available() == 0 ){}
    vetor[i] = Serial.parseInt();
    Serial.println(vetor[i]);
  }
  
  for (int i = 9; i >= 0; i--){
    Serial.print(vetor[i]);
    Serial.print("  ");
  }

  Serial.println();
}
