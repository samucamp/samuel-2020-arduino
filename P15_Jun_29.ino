//
//ALGORITMOS E ESTRUTURAS DE DADOS LISTA 02
//SAMUEL MARTINS PERES 
//
//P015
//Faca um programa para ler 3 valores numericos inteiros (A, B e C) e trocar os valores
//entre as variaveis de forma que, ao nal do programa a variavel A possua o menor valor
//e a variavel C o maior.

int A = 23;
int B = 7;
int C = 10;
int AUX = 0;

void setup() {
  Serial.begin(9600);

  if (B > C){
    AUX = C;
    C = B;
    B = AUX;
  }
  if (A > B) {
    AUX = B;
    B = A;
    A = AUX;
  } 
  if (B > C){
    AUX = C;
    C = B;
    B = AUX;
  }
  
  Serial.print("A = ");
  Serial.println(A);
  Serial.print("B = ");
  Serial.println(B);
  Serial.print("C = ");
  Serial.println(C); 
}

void loop() {
}
