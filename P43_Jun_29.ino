//
//ALGORITMOS E ESTRUTURAS DE DADOS LISTA 02
//SAMUEL MARTINS PERES 
//
//P043
//Faca um programa para ler um valor n e exibir todos os multiplos de 9 entre 1 e n. Por
//exemplo: se n = 5 o programa deve exibir 9; 18; 27; 36; 45.

int n = 7;
int value = 0;

void setup() {
  Serial.begin(9600);
  for (int i = 1; i <= n; i++) {
    Serial.println(9*i);
  }
}

void loop() {}
