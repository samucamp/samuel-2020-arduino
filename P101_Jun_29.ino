//
//ALGORITMOS E ESTRUTURAS DE DADOS LISTA 02
//SAMUEL MARTINS PERES 
//
//P101
//Faca um programa para ler lista de N nomes e idades de pessoas, onde N tambem e
//fornecido pelo usuario, e exibir o nome e a idade da pessoa mais idosa e da mais jovem.

char* myStrings[5] = {"Rafael", "Natan", "Andre", "Alexia", "Carol"};
int idade[5] = {25, 21, 22, 30, 24};
int i;
int idade_jovem = idade[0];
int idade_idosa = 0;
int jovem = 0;
int idosa = 0;

void setup() {
  Serial.begin(9600);
  
  for (i = 0; i < 5; i++){
    if (idade[i] > idade_idosa){ idade_idosa = idade[i]; idosa = i;}
    if (idade[i] < idade_jovem){ idade_jovem = idade[i]; jovem = i;}
  }
  
  
  Serial.print("A pessoa mais idosa eh: ");
  Serial.print(myStrings[idosa]);
  Serial.print(" e tem ");
  Serial.print(idade_idosa);
  Serial.println(" anos.");
  Serial.print("A pessoa mais jovem eh: ");
  Serial.println(myStrings[jovem]);
  Serial.print(" e tem ");
  Serial.print(idade_jovem);
  Serial.println(" anos.");
}

void loop() {
}

