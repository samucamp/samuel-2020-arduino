//
//ALGORITMOS E ESTRUTURAS DE DADOS LISTA 02
//SAMUEL MARTINS PERES 
//
//P132
//b[0] deve receber o maior elemento de a, enquanto que b[1] deve receber o segundo
//maior elemento de a. Voc^e pode supor que a tem pelo menos dois elementos. Resposta:
//96 89

int a[] = {32,45,89,66,12,35,10,96,38,15,13,11,65,81,35,64,16,89,54,19};
int b[2];
int aux;

void setup() {
  Serial.begin(9600);  
  
  for (int i = 0; i<20; i++){
    if (a[i] > b[0]) {b[0] = a[i]; aux = i;}
  }

  a[aux] = 0;

  for (int i = 0; i<20; i++){
    if (a[i] > b[1]) {b[1] = a[i];}
  }
  
  Serial.print("O maior valor eh: ");
  Serial.println(b[0]);
  Serial.print("O segundo maior valor eh: ");
  Serial.println(b[1]);
}

void loop() {
}
