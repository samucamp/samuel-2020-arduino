//
//ALGORITMOS E ESTRUTURAS DE DADOS LISTA 02
//SAMUEL MARTINS PERES
//
//P060
//Faça um programa para ler um número natural N e calcular o maior número primo menor do que N.

int N = 0, i = 0, aux = 0;

void setup() {
  
  Serial.begin(9600);
  delay(2000);
}

void loop() {
  Serial.print("Digite o valor de N: ");
  while( Serial.available() == 0){}
  N = Serial.parseInt();
  Serial.println(N);

  N = N - 1;
  do{   
    for (i = 1; i <= N; i++){
      if ((N % i) == 0){
        aux++;
      }
    }
    
    if (aux == 2) {
      break;
    } 
    
    aux = 0;
    
    N = N - 1;
  }while ( N > 0 );  

  Serial.print("O numero primo é ");
  Serial.println(N);
}


