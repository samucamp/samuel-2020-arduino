#include <WiFi.h>
#include <WiFiMulti.h>
WiFiMulti WiFiMulti;

const char* ssid = "network_name"; // SSID (Nome do WiFi)
const char* password = "network_password"; //Senha Wifi
const char* host = "api.thingspeak.com";
String api_key = "thingspeak_Key"; // Chave do thingSpeak APIKEY
const int temp = A0;
int tempC = 0;

void setup(){  
  Serial.begin(9600);
  Connect_to_Wifi();
}

void loop(){
  //conversão do valor medido para temperatura
  tempC = (analogRead(temp)/2048.0)*330;   
  Serial.print("Temperature: ");
  Serial.print(tempC);
  Serial.println("'C");
  delay(2000);    
  Send_Data(); // chamada de função para enviar para thingSpeak
  Serial.println("Aguarde...");
  delay(5000); //delay de 15 segundos
}

void Connect_to_Wifi(){
  WiFiMulti.addAP(ssid, password);
  Serial.println(); Serial.println();
  Serial.print("Aguardando WiFi... ");
  while (WiFiMulti.run() != WL_CONNECTED) {
    Serial.print("."); delay(500);}
  Serial.println(""); Serial.println("WiFi conectado");
  Serial.println("IP address: "); Serial.println(WiFi.localIP());
}
void Send_Data(){
  Serial.println("Preparando para enviar dados");
  // Wifi client para criar conexão TCP
  WiFiClient client;
  const int httpPort = 80;
  if (!client.connect(host, httpPort)) {
    Serial.println("connection failed");
    return;
  }else{
    String data_to_send = api_key;
    data_to_send += "&field1=";
    data_to_send += String(tempC);
    data_to_send += "\r\n\r\n";
    client.print("POST /update HTTP/1.1\n");
    client.print("Host: api.thingspeak.com\n");
    client.print("Connection: close\n");
    client.print("X-THINGSPEAKAPIKEY: " + api_key + "\n");
    client.print("Content-Type: application/x-www-form-urlencoded\n");
    client.print("Content-Length: ");
    client.print(data_to_send.length()); client.print("\n\n"); client.print(data_to_send);
    delay(1000);
  }  
  client.stop();
}
