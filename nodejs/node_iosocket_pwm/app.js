const express = require('express'); //web server
const app = express();
const server = require('http').createServer(app);
const io = require('socket.io').listen(server); //web socket server

const SerialPort = require("serialport");
const serialPort = new SerialPort("/dev/ttyUSB0", {baudRate:115200});
const Readline = require('@serialport/parser-readline');
const parser = serialPort.pipe(new Readline({delimiter: '\n' }));

server.listen(3000);

app.use(express.static('public'));

var brightness = 0;

serialPort.on("open", () => {console.log('serial port open')});

io.sockets.on('connection', function(socket) {
	socket.emit('led', {value: brightness});

	socket.on('led', function(data) {
		brightness = data.value;
		var buf = new Buffer(1);
		buf.writeUInt8(brightness, 0);
		serialPort.write(buf);
		io.sockets.emit('led', {value: brightness});

		parser.on('data', data => {console.log('got word from arduino: ', data)});
	});
});


