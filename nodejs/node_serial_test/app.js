const SerialPort = require('serialport');
const Readline = require('@serialport/parser-readline');
const port = new SerialPort('/dev/ttyUSB0', { baudRate: 9600 });
const parser = port.pipe(new Readline({ delimiter: '\n' }));

port.on("open", () => {
  console.log('serial port open');
});

setInterval(() => {
  port.write('red\n', (err) => console.log('Error on write: ', err));
console.log('message written');
}, 5000);

parser.on('data', data =>{
  console.log('got word from arduino:', data);
});
