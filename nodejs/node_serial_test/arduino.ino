String inputString = "";         // a String to hold incoming data
bool stringComplete = false;  // whether the string is complete

//Pinagem joystick
int pino_x = A2; //Pino ligado ao X do joystick
int pino_y = A3; //Pino ligado ao Y do joystick
int val_x;   //Armazena o valor do eixo X
int val_y;   //Armazena o valor do eixo Y


void setup() {
  // initialize serial:
  Serial.begin(9600);
  // reserve 200 bytes for the inputString:
  inputString.reserve(200);
}

void loop() {
  // normal loop
  printAnalog();
  // print the string when a newline arrives:
  if (stringComplete) {
    Serial.println(inputString);
    inputStringToColor();
    // clear the string:
    inputString = "";
    stringComplete = false;
  }
}

/*
  SerialEvent occurs whenever a new data comes in the hardware serial RX. This
  routine is run between each time loop() runs, so using delay inside loop can
  delay response. Multiple bytes of data may be available.
*/
void serialEvent() {
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read();
    // add it to the inputString:
    inputString += inChar;
    // if the incoming character is a newline, set a flag so the main loop can
    // do something about it:
    if (inChar == '\n') {
      stringComplete = true;
    }
  }
}

void printAnalog(){
  val_x = analogRead(pino_x);
  val_x = map(val_x, 0, 1023, 150, 255);
  Serial.println(val_x);
  delay(300);
  val_y = analogRead(pino_y);
  val_y = map(val_y, 0, 1023, 150, 255);
  Serial.println(val_y);
  delay(300);
}


void inputStringToColor() {
    if (inputString == "red\n") {
      analogWrite(11, 255);
      analogWrite(10, 0);
      analogWrite(9, 0);
    }
    else if (inputString == "green\n") {
      analogWrite(11, 0);
      analogWrite(10, 255);
      analogWrite(9, 0);
    }
    else if (inputString == "blue\n") {
      analogWrite(11, 0);
      analogWrite(10, 0);
      analogWrite(9, 255);
    }
    else if (inputString == "magenta\n") {
      analogWrite(11, 255);
      analogWrite(10, 0);
      analogWrite(9, 255);
    }
    else if (inputString == "yellow\n") {
      analogWrite(11, 255);
      analogWrite(10, 255);
      analogWrite(9, 0);  
    }
    else if (inputString== "cyan\n") {
      analogWrite(11, 0);
      analogWrite(10, 255);
      analogWrite(9, 255);
    }
    else if (inputString== "white\n") {
      analogWrite(11, 255);
      analogWrite(10, 255);
      analogWrite(9, 255);
    }
    else if (inputString == "led off\n") {
      analogWrite(11, 0);
      analogWrite(10, 0);
      analogWrite(9, 0);
    }
 }
