const app = require('http').createServer();
const io = require('socket.io').listen(app);

app.listen(port, host, (err) => {
    if (err) return console.log('Error ocurred: ' + err);
    console.log(`Server is running in ${host}:${port}`);
});

io.sockets.on('connection', function (socket) {
    socket.on('init', function () {
        socket.emit('LED', true);
    });

    socket.on('temp', function (temp) {
        console.log(temp);
    });
});