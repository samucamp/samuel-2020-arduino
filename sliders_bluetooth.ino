String stringGeral, dispositivo, valorRecebido;

int led1 = 5;
int led2 = 6;
int led3 = 7;

int dimmer;

void setup(){
	Serial.begin(9600);
	pinMode(led1, OUTPUT);
	pinMode(led2, OUTPUT);
	pinMode(led3, OUTPUT);
}

void divideDados(){
	if(stringGeral.indexOf(" ")>=0){
		dispositivo = stringGeral.substring(0,(stringGeral.indexOf(" ")));
		valorRecebido = stringGeral.substring(stringGeral.indexOf(" ")+1);
	}
}

void loop(){
	if(Serial.available()){
		stringGeral = String("");
		while(Serial.available()){
		stringGeral = stringGeral + char(Serial.read());
		delay(1);
		}
		divideDados();
                
                //Serial.println("/nString Geral: "+stringGeral);
                //Serial.println("Dispositivo: "+dispositivo);
                //Serial.println("String Geral: "+stringGeral);

		dimmer = valorRecebido.toInt();
		if(dispositivo == "dimmer1"){
		        analogWrite(led1, dimmer);
		}
		if(dispositivo == "dimmer2"){
			analogWrite(led2, dimmer);
		}
		if(dispositivo == "dimmer3"){
			analogWrite(led3, dimmer);
		}
	}
}