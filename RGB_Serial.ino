String msg;
int cor = 0;
int modo = 0;
int aux = 0;

void setup() {
  Serial.begin(9600);
  pinMode(11, OUTPUT);
  pinMode(10, OUTPUT);
  pinMode(9, OUTPUT);
}

void checkSerial() {
  String msg = "";
  if (Serial.available() > 0) {
    while (Serial.available() > 0) {
      msg += char(Serial.read());
      delay(1);
    }

    Serial.println(msg);

    if (msg == "red") {
      cor = 1;
      aux = cor;
      modo = 0;
    }
    else if (msg == "green") {
      cor = 2;
      aux = cor;
      modo = 0;
    }
    else if (msg == "blue") {
      cor = 3;
      aux = cor;
      modo = 0;
    }
    else if (msg == "magenta") {
      cor = 4;
      aux = cor;
      modo = 0;
    }
    else if (msg == "yellow") {
      cor = 5;
      aux = cor;
      modo = 0;
    }
    else if (msg == "cyan") {
      cor = 6;
      aux = cor;
      modo = 0;
    }
    else if (msg == "white") {
      cor = 7;
      aux = cor;
      modo = 0;
    }
    else if (msg == "led off") {
      cor = 0;
      modo = 0;
      aux = 0;
    }
    else if (msg == "blink mode") {
      cor = aux;
      modo = 1;
    }
    else if (msg == "fade mode") {
      cor = 8; // no color
      modo = 2;
    }
  }
}

void changeColor() {
  if (cor == 1) {
    analogWrite(11, 255);
    analogWrite(10, 0);
    analogWrite(9, 0);
  } else if (cor == 2) {
    analogWrite(11, 0);
    analogWrite(10, 255);
    analogWrite(9, 0);
  } else if (cor == 3) {
    analogWrite(11, 0);
    analogWrite(10, 0);
    analogWrite(9, 255);
  } else if (cor == 0) {
    analogWrite(11, 0);
    analogWrite(10, 0);
    analogWrite(9, 0);
  } else if (cor == 4) {
    analogWrite(11, 255);
    analogWrite(10, 0);
    analogWrite(9, 255);
  } else if (cor == 5) {
    analogWrite(11, 255);
    analogWrite(10, 255);
    analogWrite(9, 0);
  } else if (cor == 6) {
    analogWrite(11, 0);
    analogWrite(10, 255);
    analogWrite(9, 255);
  } else if (cor == 7) {
    analogWrite(11, 255);
    analogWrite(10, 255);
    analogWrite(9, 255);
  }
}

void changeMode() {
  if (modo == 1) {
    delay(50);
    analogWrite(11, 0);
    analogWrite(10, 0);
    analogWrite(9, 0);
    delay(50);
  } else if (modo == 2) {
    int r, g, b;

    for (r = 0; r <= 255 && modo == 2 ; r++) {
      analogWrite(11, r);
      analogWrite(10, g);
      analogWrite(9, b);
      delay(6);
      checkSerial();
    }
    r = 255;
    if (b > 0) {
      for (b = 255; b >= 0 && modo == 2; b--) {
        analogWrite(11, r);
        analogWrite(10, g);
        analogWrite(9, b);
        delay(6);
        checkSerial();
         b = 0;  
     }
    }
    for (g = 0; g <= 255 && modo == 2 ; g++) {
      analogWrite(11, r);
      analogWrite(10, g);
      analogWrite(9, b);
      delay(6);
      checkSerial();
    }
    g = 255;
    for (r = 255; r >= 0 && modo == 2 ; r--) {
      analogWrite(11, r);
      analogWrite(10, g);
      analogWrite(9, b);
      delay(6);
      checkSerial();
    }
    r = 0;
    for (b = 0; b <= 255 && modo == 2; b++) {
      analogWrite(11, r);
      analogWrite(10, g);
      analogWrite(9, b);
      delay(6);
      checkSerial();
    }
    b = 255;
    for (g = 255; g >= 0 && modo == 2 ; g--) {
      analogWrite(11, r);
      analogWrite(10, g);
      analogWrite(9, b);
      delay(6);
      checkSerial();
    }
    g = 0;
 } 
}

void loop() {
  checkSerial();
  changeColor();
  changeMode();
}
