void setup (){
  Serial.begin (9600); 
  delay(2000);
}

void exercicio_91(){
  int x[10] = {0};
  int i = 0;

  Serial.println ("10 valores serao armazenados em um vetor");
  
  for(i=0; i<10; i++){
    Serial.print("Digite o valor ");
    Serial.print(i);
    Serial.print(" do vetor: ");
    while (Serial.available()==0){}
    x[i] = Serial.parseInt();
    Serial.println(x[i]);
  }

  for(i=0; i<100; i++){
    Serial.print("O valor ");
    Serial.print(i);
    Serial.print(" do vetor equivale a: ");
    Serial.println(x[i]);
  }

  Serial.println();
  Serial.println();
}

void exercicio_92(){
 int x[5] = {0};
 int y[5] = {0};
 int i = 0;

 Serial.println ("Soma de dois vetores com 5 valores cada.");
 
 for(i=0; i<5; i++){
   Serial.print("Digite o valor ");
   Serial.print(i);
   Serial.print(" do vetor X: ");
   while (Serial.available()==0){}
   x[i] = Serial.parseInt();
   Serial.println(x[i]);
 }

 for(i=0; i<5; i++){
   Serial.print("Digite o valor ");
   Serial.print(i);
   Serial.print(" do vetor Y: ");
   while (Serial.available()==0){}
   y[i] = Serial.parseInt();
   Serial.println(y[i]);
 }

 for(i=0; i<5; i++){
   Serial.print("A soma da casa ");
   Serial.print(i);
   Serial.print(" dos vetores X e Y equivale a: ");
   Serial.println(x[i] + y[i]);
 }

 Serial.println();
 Serial.println();
}

void exercicio_93(){
  int x[5] = {0};
  int i = 0;
  int aux = 0;
  int imax = 0;
  int ipos = 0;

  Serial.println ("Encontrando o maior elemento de um Array");
  
  for(i=0; i<5; i++){
    Serial.print("Digite o valor ");
    Serial.print(i);
    Serial.print(" do vetor: ");
    while (Serial.available()==0){}
    x[i] = Serial.parseInt();
    Serial.println(x[i]);
  }
  
 for(i=0; i<5; i++){
   aux = x[i];
   for(i=0; i<5; i++){
     if (aux < x[i]) { imax = x[i]; ipos = i;}
   }
 }
  
 Serial.print("O maior valor do Array e: ");
 Serial.println(imax);
 Serial.print("A posicao deste valor e: ");
 Serial.println(ipos);
 Serial.println();
}

void loop () {
  exercicio_91();
//  exercicio_92();
//  exercicio_93();
}
