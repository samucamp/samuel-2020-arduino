float n1=0, n2=0, resultado=0;
int op = 0;

void setup () { Serial.begin (9600); delay(2000); }

void loop () {
  Serial.println (" Calculadora");
  
  Serial.print("Digite o valor de n1: ");
  while (Serial.available() == 0 ){}
  n1 = Serial.parseInt();
  Serial.println(n1);
    
  Serial.print("Digite o valor de n2: ");
  while (Serial.available() == 0 ){}
  n2 = Serial.parseInt();
  Serial.println(n2);
    
  Serial.print("Digite a operacao: ");
  while (Serial.available() == 0 ){}
  op = Serial.read();
  Serial.println(op);

  switch (op) {
    case '+': resultado = n1 + n2; break;
    case '-': resultado = n1 - n2; break;
    case '*': resultado = n1 * n2; break;
    case '/': resultado = n1 / n2; break;
    case '^': resultado = pow(n1,n2); break;
    default : Serial.println (" Operador invalido ");
  }
 
  Serial.print ("Resultado : ");
  Serial.println (resultado);
}
