float celsius = 0.4887585532746823069403714565;
int temperatura;
int i;
float soma;
unsigned long previousMillis = 0;  
const long interval = 1000;

void setup() {
  Serial.begin(9600);
}

void loop() {
  unsigned long currentMillis = millis();
  soma = 0;
  
  for(i=0;i<=100;i++){soma += analogRead(0);}
  temperatura = (soma/100) * celsius;

  if(currentMillis - previousMillis >= interval) {
    previousMillis = currentMillis;   
      Serial.print(temperatura);
      Serial.println("");
  }
}