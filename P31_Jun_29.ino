//
//ALGORITMOS E ESTRUTURAS DE DADOS LISTA 02
//SAMUEL MARTINS PERES 
//
//P031
//As mac~as custam R$ 1,30 cada se forem compradas menos de uma duzia, e R$ 1,00 se
//forem compradas pelo menos 12. Escreva um programa que leia o numero de mac~as
//compradas, calcule e exiba o custo total da compra.

int macas = 0;
float preco = 0;
float preco_total = 0;

void setup() {
  Serial.begin(9600);
}

void loop() {
  Serial.print("Quantas macas serao compradas? ");
  while(Serial.available() == 0 ){}
  macas = Serial.parseInt();
  Serial.println(macas);
  
  if (macas < 12) {
    preco = 1.30;
  } else {
    preco = 1.00;
  }

  preco_total = macas * preco;
  
  Serial.print("O custo total da compra eh: ");
  Serial.print(preco_total);
  Serial.println(" reais.");
  }
