# SAMUEL 2020 - Arduino

Projetos feitos em Arduino.

<br>

# Conteúdos
1. [Instalando Arduino IDE no UBUNTU](#arduino)
2. [Desinstalando](#remove)
3. [Referências](#ref)

<br>

# Instalando Arduino IDE no UBUNTU
<a name="arduino"></a>

Baixe o arquivo .tar.xz de [Arduino](https://www.arduino.cc/en/Main/Software)

Extraia o arquivo,
navegue para dentro da pasta pelo terminal,
e execute o comando abaixo.

```bash
sudo ./install.sh
```

Serão criados atalhos e ícone de aplicativo para o arquivo <code>arduino</code>.

<br>

# Desinstalando
<a name="remove"></a>

Navegue para dentro da pasta do programa pelo terminal,
e execute o comando abaixo.

```bash
sudo ./uninstall.sh
```

Exclua a pasta.

<br>

# Referências
<a name="ref"></a>

> https://www.arduino.cc/en/Main/Software
